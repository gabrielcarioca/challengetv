//
//  EndGameState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit

class EndGameState: GKState {
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is LobbyState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is RoundState:
            // How the game finishes and shows the end game statistics to all the players
            
            NSNotificationCenter.defaultCenter().postNotificationName("endGame", object: nil)
            
            
            
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
}
