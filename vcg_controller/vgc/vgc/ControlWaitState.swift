//
//  ControlWaitState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class ControlWaitState: GKState {
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is ControlGuessState.Type:
            return true
        case is ControlChooseWordState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case nil: break
        // First time the state machine is configured
        case is ControlLoginState:
            //Configues the display after master starts
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                VgcManager.elements.custom[VGCustomElementType.GameStart.rawValue]!.value = 2
                
                if let element = VgcManager.elements.custom[VGCustomElementType.GameStart.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
            }
            break
        case is ControlDrawState:
            // Configures the display after a turn happened
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                VgcManager.elements.custom[VGCustomElementType.RoundOver.rawValue]!.value = 2
                
                if let element = VgcManager.elements.custom[VGCustomElementType.RoundOver.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
            }
            break
        case is ControlGuessState:
            // Configures the display after a turn happened
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                VgcManager.elements.custom[VGCustomElementType.RoundOver.rawValue]!.value = 2
                
                if let element = VgcManager.elements.custom[VGCustomElementType.RoundOver.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
            }
            
            
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
}
