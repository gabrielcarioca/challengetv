//
//  TurnState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class TurnState: GKState {
    var timer: NSTimer!
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is RoundState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is ChooseWordState:
            // How the game organizes the turn after a word was chosen
            
            // Mark players as making guess
            for player in Game.sharedData.players {
                player.dataComponent?.gotAnswer = false
            }
            
            // Turn timer
            NSNotificationCenter.defaultCenter().postNotificationName("startTurn", object: nil)
            timer = NSTimer.scheduledTimerWithTimeInterval(Double(Game.sharedData.time), target: self, selector: #selector(self.endTurn), userInfo: nil, repeats: false)
            
            //Value Handler for custom controls
            Elements.customElements.valueChangedHandler = {(controller, element) in
                
                
                //handle the TempBoard message
                if (element.identifier == VGCustomElementType.TempBoard.rawValue) {
                    if let receivedImage = element.value as? NSData{
                        Game.sharedData.tempDrawImage = receivedImage
                        NSNotificationCenter.defaultCenter().postNotificationName("tempDrawUpdate", object: nil)
                    }
                }
                
                
                //handle the board message
                if (element.identifier == VGCustomElementType.Board.rawValue) {
                    if let receivedImage = element.value as? NSData{
                        Game.sharedData.drawImage = receivedImage
                        NSNotificationCenter.defaultCenter().postNotificationName("drawUpdate", object: nil)
                    }
                    
                }
                
                
                
                //Handle the text message (A string value)
                if (element.identifier == VGCustomElementType.TextMessage.rawValue) {
                    
                    
                    if let text = element.value as? String{
                        
                        // Right guess
                        if text.trimmedString().caseInsensitiveCompare(Game.sharedData.chosenWord!.trimmedString()) == .OrderedSame {
                            let rightGuess = "\(controller.deviceInfo.vendorName) Acertou!"
                            Game.sharedData.rightGuess = rightGuess
                            VgcManager.elements.custom[VGCustomElementType.CorrectAnswear.rawValue]!.value = 1
                            controller.sendElementStateToPeripheral(VgcManager.elements.custom[VGCustomElementType.CorrectAnswear.rawValue]!)
                            // Checks who got the correct answer and reloads table
                            for control in Game.sharedData.players{
                                if control.dataComponent?.name == controller.vendorName {
                                    control.dataComponent?.score = (control.dataComponent?.score)! + 1
                                    NSNotificationCenter.defaultCenter().postNotificationName("rightGuessUpdate", object: nil)
                                    
                                    control.dataComponent?.gotAnswer = true
                                    
                                    // Check if all players got the correct answer
                                    var howManyCorrect = 0
                                    for player in Game.sharedData.players {
                                        if player.dataComponent?.gotAnswer == true {
                                            howManyCorrect += 1
                                        }
                                    }
                                    
                                    if howManyCorrect == Game.sharedData.players.count - 1 {
                                        self.endTurn()
                                    }
                                }
                            }
                        }
                        
                        // Wrong guess
                        else {
                            let newText = "\(controller.deviceInfo.vendorName): \(text)"
                            Game.sharedData.textReceived = newText
                            NSNotificationCenter.defaultCenter().postNotificationName("wrongGuessUpdate", object: nil)
                            
                            // Sends to the controllers the wrong guess
                            VgcManager.elements.custom[VGCustomElementType.TextMessage.rawValue]!.value = newText
                            VgcController.sendElementStateToAllPeripherals(element)
                            VgcManager.elements.custom[VGCustomElementType.CorrectAnswear.rawValue]!.value = 0
                            controller.sendElementStateToPeripheral(VgcManager.elements.custom[VGCustomElementType.CorrectAnswear.rawValue]!)
                            
                        }
                    }
                }
            }
            
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
    
    func endTurn() {
        NSNotificationCenter.defaultCenter().postNotificationName("endTurn", object: nil)
        stateMachine?.enterState(RoundState)
    }
}

extension String {
    func trimmedString() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

    }
}
