//
//  ControlGuessState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class ControlGuessState: GKState {
    var timer: NSTimer!
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is ControlWaitState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is ControlWaitState:
            // Configures the display for a turn from the wait state
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                
                VgcManager.elements.custom[VGCustomElementType.Artist.rawValue]!.value = 0
                
                
                if let element = VgcManager.elements.custom[VGCustomElementType.Artist.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
                //send timer
                VgcManager.elements.custom[VGCustomElementType.Time.rawValue]!.value = Game.sharedData.time
                
                
                if let element = VgcManager.elements.custom[VGCustomElementType.Time.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
                
            }
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }

}
