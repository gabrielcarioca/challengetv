//
//  GameViewController.swift
//  vgc
//
//  Created by Gabriel Neves Ferreira on 16/06/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class GameViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var drawBoard: UIImageView!
    @IBOutlet weak var tempBoard: UIImageView!
    @IBOutlet weak var timerView: UIProgressView!
    
    @IBOutlet weak var messageBox: UITableView!
    @IBOutlet weak var playersTableView: UITableView!
    let colorArray = [UIColor.redColor(), UIColor.greenColor(), UIColor.blueColor(), UIColor.cyanColor(), UIColor.magentaColor(), UIColor.orangeColor(), UIColor.yellowColor()]
    var guesses : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playersTableView.tag = 1
        messageBox.tag = 2
        
        
       
        self.tempBoard.alpha = 0.5
        timerView.setProgress(1.0, animated: false)
        
        // Observer for Text Updates
        NSNotificationCenter.defaultCenter().addObserverForName("wrongGuessUpdate", object: nil, queue: nil) { (note) in
            self.wrongGuessUpdate()
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName("rightGuessUpdate", object: nil, queue: nil) { (note) in
            self.rightGuessUpdate()
        }
        
        // Observer for Image Updates
        NSNotificationCenter.defaultCenter().addObserverForName("drawUpdate", object: nil, queue: nil) { (note) in
            self.drawUpdate()
        }

        // Observer for Temp Image Updates
        NSNotificationCenter.defaultCenter().addObserverForName("tempDrawUpdate", object: nil, queue: nil) { (note) in
            self.tempDrawUpdate()
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName("startTurn", object: nil, queue: nil) { (note) in
            self.startTurn()
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName("endGame", object: nil, queue: nil) { (note) in
            self.endGame()
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName("endTurn", object: nil, queue: nil) { (note) in
            self.resetTurn()
        }
        
       
        
    }
    func resetTurn() {
        drawBoard.image = nil
        tempBoard.image = nil
        guesses = []
        self.messageBox.reloadData()
        self.timerView.setProgress(1.0, animated: false)
    }

    func tempDrawUpdate() {
        
        print("recebimento temporario")
        if let receivedImage = Game.sharedData.tempDrawImage {
            self.tempBoard.image = UIImage(data: receivedImage)
        }
    }
    
    func drawUpdate() {
        print("recebimento")
        
        if let receivedImage = Game.sharedData.drawImage {
            self.drawBoard.image = UIImage(data: receivedImage)
        }
        
    }
    
    func wrongGuessUpdate() {
        if let textReceived = Game.sharedData.textReceived {
            self.guesses.append(textReceived)
            
          
            self.messageBox.reloadData()

            
            let indexPath = NSIndexPath(forRow: self.messageBox.numberOfRowsInSection(self.messageBox.numberOfSections-1)-1, inSection: (self.messageBox.numberOfSections-1))
            self.messageBox.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
    

        }
    }
    
    func rightGuessUpdate() {
        if let textReceived = Game.sharedData.rightGuess {
            
            self.guesses.append(textReceived)

            self.playersTableView.reloadData()
            self.messageBox.reloadData()
            let indexPath = NSIndexPath(forRow: self.messageBox.numberOfRowsInSection(self.messageBox.numberOfSections-1)-1, inSection: (self.messageBox.numberOfSections-1))
            self.messageBox.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)

            
        }
    }
    
    func startTurn() {
        drawBoard.image = nil
        tempBoard.image = nil
        guesses = []
        self.messageBox.reloadData()
        self.timerView.setProgress(1.0, animated: false)
        UIView.animateWithDuration(Double(Game.sharedData.time), animations: { () -> Void in
            self.timerView.setProgress(0.0, animated: true)
        })
    
        
    }
    
    func endGame() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.performSegueWithIdentifier("gameEndSegue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == playersTableView{
            
            return tableView.frame.height / 8
            
        }else{
            
            return tableView.frame.height / 8
        }
        
       
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == playersTableView.tag{
            return Game.sharedData.players.count
        }else{
            print(guesses.count)
             return guesses.count
            
        }
    }
    
    // MARK: - Table View DataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var tableViewCell :UITableViewCell
        
        if tableView.tag == playersTableView.tag{
        
        tableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "playerCell"+"\(indexPath)")
        tableViewCell.textLabel?.text = (Game.sharedData.players[indexPath.row].dataComponent?.name)! + ": \(Game.sharedData.players[indexPath.row].dataComponent!.score!) acertos"
        tableViewCell.textLabel?.font = UIFont(name: "Helvetica", size: 35)
            
        }else{
            tableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "guess"+"\(indexPath)")
            tableViewCell.textLabel?.text = guesses[indexPath.row]
            print(tableViewCell.textLabel?.text)
            tableViewCell.textLabel?.font = UIFont(name: "Helvetica", size: 35)
            
        }
        return tableViewCell
    }

    
    
    
    
}
