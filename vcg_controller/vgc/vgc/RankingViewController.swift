//
//  RankingViewController.swift
//  vgc
//
//  Created by Caio Vinícius Piologo Véras Fernandes on 6/20/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class RankingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var table: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // State is responsible for managing all that happens on screen for now.
        Game.sharedData.gameStateMachine.enterState(EndGameState)

        //Waits about 4 seconds to go back to lobby
        _ = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: #selector(RankingViewController.backToLobby), userInfo: nil, repeats: false)
        table.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    func backToLobby(){
        self.performSegueWithIdentifier("lobbyGameSegue", sender: self)
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.height / 8
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Game.sharedData.players.count
    }
    // MARK: - Table View DataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "playerCell"+"\(indexPath)")
        //Game.sharedData.players.sortInPlace({$0.dataComponent?.score > $1.dataComponent?.score})
        tableViewCell.textLabel?.text = Game.sharedData.players[indexPath.row].dataComponent?.name
        tableViewCell.textLabel?.font = UIFont(name: "Helvetica", size: 35)
        
        return tableViewCell
    }
}