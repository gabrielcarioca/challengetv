//
//  ControlManagerComponent.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class ControlManagerComponent: GKComponent {
    weak var player: PlayerEntity? {
        get {
            return self.entity as? PlayerEntity
        }
    }
    
    var controlStateMachine: ControlStateMachine!
    var controller: VgcController!
    var master : Bool = false
    var isDrawing: Bool = false
    
    init(control: VgcController) {
        super.init()
        let drawState = ControlDrawState()
        let loginState = ControlLoginState()
        let guessState = ControlGuessState()
        let waitState = ControlWaitState()
        let chooseState = ControlChooseWordState()
        
        self.controller = control

        controlStateMachine = ControlStateMachine(states: [drawState, guessState, waitState, chooseState, loginState], controlComponent: self)
    }
    
    
}
