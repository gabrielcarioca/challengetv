//
//  LobbyViewController.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class LobbyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var playersTableView: UITableView!
    let colorArray = [UIColor.redColor(), UIColor.greenColor(), UIColor.blueColor(), UIColor.cyanColor(), UIColor.magentaColor(), UIColor.orangeColor(), UIColor.yellowColor()]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // State is responsible for managing all that happens on screen for now.
        Game.sharedData.gameStateMachine.enterState(LobbyState)
        
        // Add observer for changing the screen when game starts
        NSNotificationCenter.defaultCenter().addObserverForName("gameStart", object: nil, queue: nil) { (note) in
            self.gameStart()
        }
        
        // Observer for table updates
        NSNotificationCenter.defaultCenter().addObserverForName("tableUpdate", object: nil, queue: nil) { (note) in
            self.playersTableView.reloadData()
        }
        
        playersTableView.userInteractionEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gameStart() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.performSegueWithIdentifier("gameStartSegue", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table View Delegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.height / 8
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Game.sharedData.players.count
    }
    
    // MARK: - Table View DataSource

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "playerCell"+"\(indexPath)")
        tableViewCell.textLabel?.text = Game.sharedData.players[indexPath.row].dataComponent?.name
        tableViewCell.textLabel?.font = UIFont(name: "Helvetica", size: 35)
        
        return tableViewCell
    }
}
