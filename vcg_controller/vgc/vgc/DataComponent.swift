//
//  DataComponent.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit

class DataComponent: GKComponent {
    weak var player: PlayerEntity? {
        get {
            return self.entity as? PlayerEntity
        }
    }

    var name: String!
    var score: Int!
    var gotAnswer: Bool = false
    
    init(name: String) {
        super.init()
        self.name = name
        score = 0
    }
    
}
