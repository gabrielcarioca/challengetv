//
//  ControlChooseWordState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/16/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class ControlChooseWordState: GKState {
    var timer: NSTimer!

    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is ControlDrawState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is ControlWaitState:
            // Configures the display after a turn happened
            timer = NSTimer.scheduledTimerWithTimeInterval(Double(1), target: self, selector: #selector(self.handshake), userInfo: nil, repeats: false)

            
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
    
    
    
    func handshake() {
        
        if let controlStateMachine = stateMachine as? ControlStateMachine {
            
            
            VgcManager.elements.custom[VGCustomElementType.Artist.rawValue]!.value = 1
            
            
            if let element = VgcManager.elements.custom[VGCustomElementType.Artist.rawValue] {
                controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
            }
            
            //send word
            let word = Game.sharedData.generateWord()
            
            VgcManager.elements.custom[VGCustomElementType.ChosenWord.rawValue]!.value = word
            
            
            if let element = VgcManager.elements.custom[VGCustomElementType.ChosenWord.rawValue] {
                controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
            }
            
            
        }
        
        
    }
    
    

}
