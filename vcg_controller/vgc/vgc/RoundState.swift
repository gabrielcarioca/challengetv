//
//  RoundState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit

class RoundState: GKState {
    var playerOrder: [Int] = [Int]()
    
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is ChooseWordState.Type:
            return true
        case is EndGameState.Type:
            return true
        default:
            return false
        }
    }
    
    override init() {
        super.init()
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is LobbyState:
            // How the game organizes the round when entering when first launching the game
            
            // Create a round order for the players
            playerOrder = (0...Game.sharedData.players.count - 1).map{ $0 }
            playerOrder.shuffleInPlace()
            
            // Make all the players wait
            for player in Game.sharedData.players {
                player.controlComponent?.controlStateMachine.enterState(ControlWaitState)
                player.controlComponent?.isDrawing = false

            }
            
            // Make the first random player on the defined order start choosing word
            Game.sharedData.players[playerOrder.first!].controlComponent!.controlStateMachine.enterState(ControlChooseWordState)
            Game.sharedData.players[playerOrder.first!].controlComponent!.isDrawing = true

            // Identify this turn as already started
            playerOrder.removeFirst()
            
            // Enter the Choose Word State
            self.stateMachine?.enterState(ChooseWordState)
            break
            
        case is TurnState:
            // How the round keeps the game going after all players had their turn.

            // Make all the players wait
            for player in Game.sharedData.players {
                player.controlComponent?.controlStateMachine.enterState(ControlWaitState)
                player.controlComponent?.isDrawing = false

            }
            
            // Make the first random player on the defined order start choosing word
            if let currentPlayer = playerOrder.first {
                Game.sharedData.players[currentPlayer].controlComponent!.controlStateMachine.enterState(ControlChooseWordState)
                Game.sharedData.players[currentPlayer].controlComponent!.isDrawing = true
                
                // Identify this turn as already started
                playerOrder.removeFirst()
                
                // Enter the Choose Word State
                self.stateMachine?.enterState(ChooseWordState)
            }
            
            // There is no more players to play, end the game
            else {
                self.stateMachine?.enterState(EndGameState)
            }
            
            break
            
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
}

// Shuffle code for an array ( http://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift )

extension CollectionType {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}
