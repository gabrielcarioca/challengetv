//
//  Game.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit

class Game {
    static let sharedData = Game()
    var gameStateMachine: GKStateMachine
    var players: [PlayerEntity]
    var chosenWord: String?
    let time = 60
    
    var tempDrawImage: NSData?
    var drawImage: NSData?
    var textReceived: String?
    var rightGuess: String?
    
    private init() {
        players = [PlayerEntity]()
        
        let lobby = LobbyState()
        let chooseWord = ChooseWordState()
        let round = RoundState()
        let turn = TurnState()
        let endGame = EndGameState()
        
        gameStateMachine = GKStateMachine(states: [lobby, chooseWord, round, turn, endGame])
        gameStateMachine.enterState(LobbyState)
    }
    
    func generateWord() -> String {
        //code to randomize
        var words = [String]()
        words.append("Heart")
        words.append("Arm")
        words.append("Snake")
        words.append("Bracelet")
        words.append("Witch")
        words.append("Cross")
        words.append("Sacrifice")
        words.append("Glass")
        words.append("Dawn")
        words.append("Frigideira")
        words.append("Soda")
        words.append("Diaper")
        words.append("Chicken")
        words.append("Cold")
        words.append("Scream")
        words.append("Church")
        words.append("Grilo")
        words.append("Tie")
        words.append("Toilet")
        words.append("Printer")
        words.append("Teacher")
        words.append("Wheat")
        
        let randomIndex = arc4random_uniform(UInt32(words.count))
        
        Game.sharedData.chosenWord = words[Int(randomIndex)]

        
        return Game.sharedData.chosenWord!
    }
    
    
    
}
