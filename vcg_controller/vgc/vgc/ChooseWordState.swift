//
//  ChooseWordState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class ChooseWordState: GKState {
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is TurnState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is RoundState:
            // How the game configures choosing words the first time of the round
            
            //2 - Set observers for controller connection/disconnection
            // TODO - Add disconection handler someday
            //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.controllerDidConnect(_:)), name: VgcControllerDidConnectNotification, object: nil)
            
            //Handler for starting a turn
            Elements.customElements.valueChangedHandler = { [unowned self](controller, element) in
                
                if (element.identifier == VGCustomElementType.RoundStart.rawValue) {
                    
                    //Sends to all mobiles to start game and go to wait screen
                    for player in Game.sharedData.players {
                        if player.controlComponent?.isDrawing == true {
                            player.controlComponent?.controlStateMachine.enterState(ControlDrawState)
                        }
                        
                        else {
                            player.controlComponent?.controlStateMachine.enterState(ControlGuessState)
                        }
                    }
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("gameStart", object: nil)
                    self.stateMachine?.enterState(TurnState)
                }
            }

            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState from other classes.
            break
        }
    }
}
