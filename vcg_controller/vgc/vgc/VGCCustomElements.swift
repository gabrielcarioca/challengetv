//
//  CustomControls.swift
//  vgc
//
//  Created by SERGIO J RAFAEL ORDINE on 6/7/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//


import VirtualGameController

public enum VGCustomElementType: Int {
    
    case CorrectAnswear   = 50
    case TextMessage = 51
    case Board = 52
    case TempBoard = 53
    case GameMaster = 54
    case GameStart = 55
    case Artist = 56
    case RoundOver = 57
    case ChosenWord = 58
    case WordsAvailable = 59
    case Time = 60
    case GameOver = 61
    case RoundStart = 62
    case Handshake = 63
    
    
    
}

///
/// Your customElements class must descend from CustomElementsSuperclass
///
public class VGCCustomElements: CustomElementsSuperclass {
    
    public override init() {
        
        super.init()
        
        customProfileElements = [
            CustomElement(name: "TextMessage", dataType: .String, type:VGCustomElementType.TextMessage.rawValue),
            CustomElement(name: "CorrectAnswear", dataType: .Int, type:VGCustomElementType.CorrectAnswear.rawValue),
            CustomElement(name: "Board", dataType: .Data, type:VGCustomElementType.Board.rawValue),
            CustomElement(name: "TempBoard", dataType: .Data, type:VGCustomElementType.TempBoard.rawValue),
            CustomElement(name: "GameMaster", dataType: .Int, type:VGCustomElementType.GameMaster.rawValue),
            CustomElement(name: "GameStart", dataType: .Int, type:VGCustomElementType.GameStart.rawValue),
            CustomElement(name: "Artist", dataType: .Int, type:VGCustomElementType.Artist.rawValue),
            CustomElement(name: "RoundOver", dataType: .Int, type:VGCustomElementType.RoundOver.rawValue),
            CustomElement(name: "ChosenWord", dataType: .String, type:VGCustomElementType.ChosenWord.rawValue),
            CustomElement(name: "WordsAvailable", dataType: .Data, type:VGCustomElementType.WordsAvailable.rawValue),
            CustomElement(name: "Time", dataType: .Int, type:VGCustomElementType.Time.rawValue),
            CustomElement(name: "GameOver", dataType: .Int, type:VGCustomElementType.GameOver.rawValue),
            CustomElement(name: "RoundStart", dataType: .Int, type:VGCustomElementType.RoundStart.rawValue),
            CustomElement(name: "Handshake", dataType: .Int, type:VGCustomElementType.Handshake.rawValue),




            
            
        ]
    }
    
}