//
//  ControlStateMachine.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/17/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import GameplayKit

class ControlStateMachine: GKStateMachine {
    weak var controlComponent: ControlManagerComponent?
    
    init(states: [GKState], controlComponent: ControlManagerComponent) {
        super.init(states: states)
        self.controlComponent = controlComponent
    }
}
