//
//  ControlLoginState.swift
//  vgc
//
//  Created by Gabriel Neves Ferreira on 17/06/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import GameplayKit
import VirtualGameController

class ControlLoginState: GKState {
    
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is ControlWaitState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case is ControlWaitState:
            // Configures the display after a turn happened
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                    
                VgcManager.elements.custom[VGCustomElementType.GameOver.rawValue]!.value = 1
                
                if let element = VgcManager.elements.custom[VGCustomElementType.GameOver.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
            }
            
            break
        case nil:
            //Configures the display on the first login
            if let controlStateMachine = stateMachine as? ControlStateMachine {
                if controlStateMachine.controlComponent?.master == true{
                    
                    VgcManager.elements.custom[VGCustomElementType.GameMaster.rawValue]!.value = 1

                    
                }else{
                    
                    VgcManager.elements.custom[VGCustomElementType.GameMaster.rawValue]!.value = 0

                }
                
                if let element = VgcManager.elements.custom[VGCustomElementType.GameMaster.rawValue] {
                    controlStateMachine.controlComponent?.controller.sendElementStateToPeripheral(element)
                }
            }
            break;
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }

}
