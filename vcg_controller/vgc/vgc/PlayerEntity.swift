//
//  playerEntity.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/14/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController

class PlayerEntity: GKEntity {
    weak var controlComponent: ControlManagerComponent? {
        get{
            return self.componentForClass(ControlManagerComponent)
        }
    }
    
    weak var dataComponent: DataComponent? {
        get {
            return self.componentForClass(DataComponent)
        }
    }
    
    init(controller: VgcController) {
        super.init()
        
        let controlCompoent = ControlManagerComponent(control: controller)
        addComponent(controlCompoent)
        
        let dataComponent = DataComponent(name: controller.vendorName)
        addComponent(dataComponent)
    }
    
    
}
