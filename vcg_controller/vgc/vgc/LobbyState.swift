//
//  LobbyState.swift
//  vgc
//
//  Created by Augusto Miranda Garcia on 6/13/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import GameplayKit
import VirtualGameController


class LobbyState: GKState {
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is RoundState.Type:
            return true
        default:
            return false
        }
    }
    
    var x = true
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        switch previousState {
        case nil:
            // How the game starts and configures the lobby from the game start
            
            //1 - Publishes the central service
            VgcManager.startAs(.Central, appIdentifier: "neves", customElements: VGCCustomElements(), customMappings: CustomMappings())
            
            //2 - Set observers for controller connection/disconnection
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.controllerDidConnect(_:)), name: VgcControllerDidConnectNotification, object: nil)
            
            //Handler for start game
            Elements.customElements.valueChangedHandler = { [unowned self](controller, element) in
                
                //handle the gameStart message
                if (element.identifier == VGCustomElementType.GameStart.rawValue) {
                    
                    //Sends to all mobiles to start game and go to wait screen
                    for player in Game.sharedData.players {
                        player.controlComponent?.controlStateMachine.enterState(ControlWaitState)
                    }
                    NSNotificationCenter.defaultCenter().postNotificationName("gameStart", object: nil)
                    self.stateMachine?.enterState(RoundState)
                }
            }
            
            
            break
        case is EndGameState:
            // How the game starts if it reached an end before
            break
        default:
            // No need for coding here, the entering states should have already benn defined in isValidNextState
            break
        }
    }
    
    // MARK: Connection Notification Methods
    @objc func controllerDidConnect(notification: NSNotification) {
        // Checks if the object conforms to the protocol in the new controller
        guard let newController: VgcController = notification.object as? VgcController else {
            return
        }
        //check if real controller
        
        if x == true{
        
            x = false
        }else{
        if newController.deviceInfo.profileType == .ExtendedGamepad{
            // Adds new player to the array of players
            if Game.sharedData.players.count < 8 {
                let newPlayer = PlayerEntity(controller: newController)
                //checks if the player is player1 and send master or not
                if Game.sharedData.players.isEmpty{
                    
                    newPlayer.controlComponent?.master = true
                    
                }
                Game.sharedData.players.append(newPlayer)
                NSNotificationCenter.defaultCenter().postNotificationName("tableUpdate", object: nil)
                
               
                
                newPlayer.controlComponent?.controlStateMachine.enterState(ControlLoginState)

                
            }
        }
        
        print ("Conectou!! \(newController)")
        }
        
    }

}
