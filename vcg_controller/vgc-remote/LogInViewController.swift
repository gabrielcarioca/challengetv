//
//  LogInViewController.swift
//  vgc-remote
//
//  Created by Gabriel Neves Ferreira on 15/06/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class LogInViewController: UIViewController{

    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var connectedFeedback: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Adds recognizer to the view to disable the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // connectButton configuration
        connectButton.layer.cornerRadius = 5
        connectButton.layer.borderWidth = 1
        connectButton.layer.borderColor = UIColor.blackColor().CGColor
        
        //Vgc initilization - publishes the peripheral device for the tv
        VgcManager.startAs(.Peripheral, appIdentifier: "neves", customElements: VGCCustomElements(), customMappings: CustomMappings())
        VgcManager.peripheral.deviceInfo = DeviceInfo(deviceUID: "", vendorName: "", attachedToDevice: false, profileType: .ExtendedGamepad, controllerType: .Software, supportsMotion: true)
        
        
        // Services methods for connecting the peripherial
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LogInViewController.foundService(_:)), name: VgcPeripheralFoundService, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LogInViewController.peripheralDidConnect(_:)), name: VgcPeripheralDidConnectNotification, object: nil)

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundIphone.png")!)
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        VgcManager.peripheral.browseForServices()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
        
    }
    
    /// Connects to the device, setting the name for the deviceInfo that is used as identification on screen.
    @IBAction func connect(sender: AnyObject) {
        dismissKeyboard()

        // Changes name and connects to service if available
        VgcManager.peripheral.deviceInfo.setValue(usernameText.text, forKey: "vendorName")
        
        if(VgcManager.peripheral.availableServices.count > 0){
            
            VgcManager.peripheral.connectToService(VgcManager.peripheral.availableServices[0])
            
            
    
        }
    }
    
    //MARK: Peripheral services
    func peripheralDidConnect(notification: NSNotification) {
        // Control was able to connect to central
        
        // Stop browsing other services.
        VgcManager.peripheral.stopBrowsingForServices()
        
        
        
        // Waits for decision if master
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.GameMaster.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                                
                if let master = element.value as? Int{
                    print(master)
                    if master == 1{
                        //show game start buttom
                        self.startButton.hidden = false
                        
                    }
                    self.connectedFeedback.hidden = false
                    self.connectButton.hidden = true
                    
                }
            }
        }
        
        
        // Waits for decision if start
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.GameStart.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                self.performSegueWithIdentifier("waitScreenSegue", sender: self)
                
                
            }
        }
        
     
    }
    
    func foundService(notification: NSNotification) {
        //enable logButton and remove turn on TV App
        let vgcService = notification.object as? VgcService
        print("Found \(vgcService?.fullName)")
    }
    
    
    @IBAction func masterStart(sender: AnyObject) {
        
        VgcManager.elements.custom[VGCustomElementType.GameStart.rawValue]!.value = 1
        VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.GameStart.rawValue]!)
        
    }


}
