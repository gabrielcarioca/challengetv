//
//  ViewController.swift
//  vgc-remote
//
//  Created by SERGIO J RAFAEL ORDINE on 6/7/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class ViewController: UIViewController{
    
    @IBOutlet weak var userText: UITextField!


    override func viewDidLoad() {

        super.viewDidLoad()
        
        //adds recognizer to the view to disable the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
       
        
        //1 - Publishes the peripheral device for the tv
        VgcManager.startAs(.Peripheral, appIdentifier: "neves", customElements: VGCCustomElements(), customMappings: CustomMappings())
        
        VgcManager.peripheral.deviceInfo = DeviceInfo(deviceUID: "", vendorName: "", attachedToDevice: false, profileType: .ExtendedGamepad, controllerType: .Software, supportsMotion: true)
       

        //3 - Service lookup methods
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.foundService(_:)), name: VgcPeripheralFoundService, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.peripheralDidConnect(_:)), name: VgcPeripheralDidConnectNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.peripheralDidDisconnect(_:)), name: VgcPeripheralDidDisconnectNotification, object: nil)
        

    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        VgcManager.peripheral.browseForServices()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Service lookup observers
    @objc func foundService(notification: NSNotification) {
        let vgcService = notification.object as? VgcService
        
       
        
        print("Found \(vgcService?.fullName)")
    }
    
    @objc func peripheralDidConnect(notification: NSNotification) {
        // Control was able to connect to central
        // stop browsing other services.
        VgcManager.peripheral.stopBrowsingForServices()
        print("Peripheral did connected to: \(notification.name)")
        
        
        //Move to controller screen
    }
    
    @objc func peripheralDidDisconnect(notification: NSNotification) {
        print("Peripheral did disconnected to: \(notification.name)")
        VgcManager.peripheral.browseForServices()

    }
    
    
    @IBAction func buttonLog(sender: AnyObject) {
        
        VgcManager.peripheral.deviceInfo.setValue(userText.text, forKey: "vendorName")
        
        print(VgcManager.peripheral.deviceInfo.vendorName)
        
        if(VgcManager.peripheral.availableServices.count > 0){
        
            VgcManager.peripheral.connectToService(VgcManager.peripheral.availableServices[0])
            
        }
        performSegueWithIdentifier("ControllerSegue", sender: self)
    }
    
    @IBAction func drawbuttonLog(sender: AnyObject) {
        
        VgcManager.peripheral.deviceInfo.setValue(userText.text, forKey: "vendorName")
        
        print(VgcManager.peripheral.deviceInfo.vendorName)
        
        if(VgcManager.peripheral.availableServices.count > 0){
            
            VgcManager.peripheral.connectToService(VgcManager.peripheral.availableServices[0])
            
        }
        
    }
    
    
    
    
    func dismissKeyboard() {
        
        view.endEditing(true)
        
    }
    
    
}

