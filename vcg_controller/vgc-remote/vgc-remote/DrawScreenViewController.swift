//
//  DrawScreenViewController.swift
//  vgc-remote
//
//  Created by Augusto Miranda Garcia on 6/9/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController


class DrawScreenViewController: UIViewController {

    @IBOutlet weak var drawBoard: UIImageView!
    @IBOutlet weak var tempBoard: UIImageView!
    
    var timer = 0

    // Boolean Identifier
    var swiped = false
    var lastPoint = CGPointZero
    
    let brushWidth: CGFloat = 10.0
    var brushColor = UIColor.blackColor()
    var drawOpacity: CGFloat = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(drawBoard.frame.height)
        print(drawBoard.frame.width)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Waits for round over
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.RoundOver.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
               
                //goes to wait segue
                let viewsToPop = 2
                var viewControllers = self.navigationController?.viewControllers
                viewControllers?.removeLast(viewsToPop)
                self.navigationController?.setViewControllers(viewControllers!, animated: true)
            }
        }
        
        //handler for timer
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.Time.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                if let timerValue = element.value as? Int{
                    
                    self.timer = timerValue
                    
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func drawBegan(sender: AnyObject) {
        brushColor = UIColor.blackColor()
        drawOpacity = 0.5
    }

    @IBAction func eraseBegan(sender: AnyObject) {
        brushColor = UIColor.whiteColor()
        drawOpacity = 1.0
    }
    
    @IBAction func clearBegan(sender: AnyObject) {
        tempBoard.image = nil
        drawBoard.image = nil
        self.sendBoard(drawBoard)
        self.sendTempBoard(tempBoard)
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.locationInView(self.view)
        }
    }
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        let context = UIGraphicsGetCurrentContext()
        tempBoard.image?.drawInRect(CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        
        CGContextMoveToPoint(context, fromPoint.x, fromPoint.y)
        CGContextAddLineToPoint(context, toPoint.x, toPoint.y)
        
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineWidth(context, brushWidth)
        var red, green, blue, alpha: CGFloat
        red = 0.0
        green = 0.0
        blue = 0.0
        alpha = 0.0
        brushColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        CGContextSetRGBStrokeColor(context, red, green, blue, alpha)
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        CGContextStrokePath(context)
        
        tempBoard.image = UIGraphicsGetImageFromCurrentImageContext()
        tempBoard.alpha = drawOpacity
        UIGraphicsEndImageContext()
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        swiped = true
        if let touch = touches.first{
            let currentPoint = touch.locationInView(view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            lastPoint = currentPoint
            
            sendTempBoard(tempBoard)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !swiped {
            // draw a single point
            drawLineFrom(lastPoint, toPoint: lastPoint)
        }
        
        // Merge tempImageView into mainImageView
        UIGraphicsBeginImageContext(drawBoard.frame.size)
        drawBoard.image?.drawInRect(CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), blendMode: CGBlendMode.Normal, alpha: 1.0)
        tempBoard.image?.drawInRect(CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), blendMode: CGBlendMode.Normal, alpha: 1.0)
        drawBoard.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        
        sendBoard(drawBoard)
        sendTempBoard(tempBoard)
    }
    
    func sendBoard(board : UIImageView){
        
        
        if board.image != nil{
            
                print("envio")
            
            VgcManager.elements.custom[VGCustomElementType.Board.rawValue]!.value = UIImagePNGRepresentation(board.image!)!
            VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.Board.rawValue]!)
        }
        
        
    }
    
    func sendTempBoard(tempboard : UIImageView){
        
        //Send a tempBoard message to central
        if tempboard.image != nil{

            VgcManager.elements.custom[VGCustomElementType.TempBoard.rawValue]!.value = UIImagePNGRepresentation(tempboard.image!)!
            VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.TempBoard.rawValue]!)
        }
        
    }
  
    
}
