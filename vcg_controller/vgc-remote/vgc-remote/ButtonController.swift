//
//  ButtonController.swift
//  vgc-remote
//
//  Created by Matheus Rocco Ferreira on 6/7/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import VirtualGameController


class ButtonController: UIViewController {
    
    @IBOutlet var textMessage: UITextField!
    @IBOutlet weak var messageBox: UITextView!




    @IBAction func sendMessage(sender: AnyObject) {
        
        //Send a custom text message to central (String value)
        VgcManager.elements.custom[VGCustomElementType.TextMessage.rawValue]!.value = textMessage.text!
        VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.TextMessage.rawValue]!)
        
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //adds recognizer to the view to disable the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
     

        
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.CorrectAnswear.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                print("Custom element handler fired for Send Acerto")
                
                
            }
        }
        
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.TextMessage.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                
                if let text = element.value as? String{
                    
                    self.messageBox.text.appendContentsOf(text)
                    
                }
                
            }
        }
 
        
        
    }
    
    
    
    
    func dismissKeyboard() {
        
        view.endEditing(true)
        
    }
    

 
  
    
    
}
