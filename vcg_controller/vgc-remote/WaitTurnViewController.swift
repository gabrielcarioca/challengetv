//
//  WaitTurnViewController.swift
//  vgc-remote
//
//  Created by Augusto Miranda Garcia on 6/15/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class WaitTurnViewController: UIViewController {
    
    var readyForWord = false
    var chosenWord : String?
    @IBOutlet weak var activityControler: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.    
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundIphone.png")!)
        activityControler.startAnimating()
        // Waits for command if drawing
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.Artist.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                if let artist = element.value as? Int{
                    
                    //1 if artist 0 if guessing
                    if artist == 1{
                        self.readyForWord = true

                    }else{
                        self.activityControler.stopAnimating()
                        self.performSegueWithIdentifier("GuessingSegue", sender: self)
                        
                    }
                }
            }
        }
        
        // Waits for command if game over
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.GameOver.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                if let gameOver = element.value as? Int{
                    
                    //1 if artist 0 if guessing
                    if gameOver == 1{
                        self.activityControler.stopAnimating()
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
        }
        
        
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.ChosenWord.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                if let word = element.value as? String{
                    
                    self.chosenWord = word
                    
                    if self.readyForWord{
                        self.activityControler.stopAnimating()
                        self.performSegueWithIdentifier("ArtistSegue", sender: self)
                    }
                    
                   
                    
                }
            }
        }
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.destinationViewController is ChooseWordViewController{
            (segue.destinationViewController as! ChooseWordViewController).word = self.chosenWord
        }
       
    }
 

}
