//
//  ChooseWordController.swift
//  vgc-remote
//
//  Created by Gabriel Neves Ferreira on 17/06/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//

import UIKit
import VirtualGameController

class ChooseWordViewController: UIViewController {
    @IBOutlet weak var chosenWord: UILabel!
    var word : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundIphone.png")!)
        
        // Waits for chosen word
       
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func showWordClicked(sender: AnyObject) {
        
        //shows the word
        chosenWord.text = word
        chosenWord.hidden = false
        
    }
    @IBAction func showWordDisClicked(sender: AnyObject) {
        
        //hides the word
        chosenWord.hidden = true
        
    }
    
    @IBAction func drawClicked(sender: AnyObject) {
        
        //sends
        VgcManager.elements.custom[VGCustomElementType.RoundStart.rawValue]!.value = 1
        VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.RoundStart.rawValue]!)
        
        //go to drawScreen
        self.performSegueWithIdentifier("DrawSegue", sender: self)

        
    }
}