//
//  guessScreenViewController.swift
//  vgc-remote
//
//  Created by Gabriel Neves Ferreira on 14/06/16.
//  Copyright © 2016 BEPiD. All rights reserved.
//


import UIKit
import VirtualGameController

class GuessScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var sendButtom: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var messageBox: UITableView!

    @IBOutlet weak var guessTextField: UITextField!
    var guesses : [String] = []

    
    var timer = 0
    var constraint : CGFloat = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Do any aditional layout setup
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundIphone.png")!)

        
        //adds recognizer to the view to disable the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        //notifications to when the keyboard appears
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GuessScreenViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GuessScreenViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        //sendbuttom configs
        sendButtom.layer.cornerRadius = 5
        sendButtom.layer.borderWidth = 1
        sendButtom.layer.borderColor = UIColor.blackColor().CGColor
        
        
        //configure handlers to receibe broadcast or correct answear or end of round
        //handler correct answear
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.CorrectAnswear.rawValue){
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                
                if let value = element.value as? Int{
                    print(value)
                    if value == 1{
                        self.gotCorrectAnswear()

                        
                    }else{
                        self.wrongAnswear()
                    }
                    
                }
                
                
            }
        }
        //handler de mensagem
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.TextMessage.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
               //funcao de adicionar balaozinho
                if let text = element.value as? String{
                    
                    self.guesses.append(text)
                    self.messageBox.reloadData()
                    
                    
                    let indexPath = NSIndexPath(forRow: self.messageBox.numberOfRowsInSection(self.messageBox.numberOfSections-1)-1, inSection: (self.messageBox.numberOfSections-1))
                    self.messageBox.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                    
                }
                
                
            }
        }
        
        //handler for round over
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.RoundOver.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                //goes to waitScreen
                self.navigationController?.popViewControllerAnimated(true)

                
            }
        }
        
        //handler for timer
        if let element: Element = VgcManager.elements.elementFromIdentifier(VGCustomElementType.Time.rawValue){
            
            element.valueChangedHandlerForPeripheral = { (element: Element) in
                
                if let timerValue = element.value as? Int{
                    
                    self.timer = timerValue
                    
                }
                
                
            }
        }
        
        
        //outros handlers
        
        
        constraint = self.bottomConstraint.constant
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    func dismissKeyboard() {
        view.endEditing(true)
        
    }
    

    
    @IBAction func sendGuess(sender: UIButton) {

        VgcManager.elements.custom[VGCustomElementType.TextMessage.rawValue]!.value = guessTextField.text!
        VgcManager.peripheral.sendElementState(VgcManager.elements.custom[VGCustomElementType.TextMessage.rawValue]!)
        
        guessTextField.text = ""
        
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        
        if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            
            UIView.beginAnimations(nil, context: nil)
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            let changeInHeight = CGRectGetHeight(keyboardSize)
            UIView.animateWithDuration(animationDurarion, animations: { () -> Void in
                if self.bottomConstraint.constant < changeInHeight{
                    self.bottomConstraint.constant += changeInHeight
                }
                
            })
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        
        if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            
            UIView.beginAnimations(nil, context: nil)
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            let changeInHeight = CGRectGetHeight(keyboardSize)
                //5
                UIView.animateWithDuration(animationDurarion, animations: { () -> Void in
                    self.bottomConstraint.constant -= changeInHeight
                })
            
            
        }
    }
    
    func gotCorrectAnswear(){
        
        
        sendButtom.enabled = false
        sendButtom.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        sendButtom.alpha = 0.2
        guessTextField.enabled = false
        guessTextField.backgroundColor = UIColor.lightGrayColor()
        guessTextField.alpha = 0.2
        correctLabel.alpha = 1
        
    }
    
    
    func wrongAnswear(){
        
    }
    
    
    // MARK: - Table View Delegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.height/10
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return guesses.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.clearColor()
    }
        
    
    // MARK: - Table View DataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        var tableViewCell :UITableViewCell
        
        tableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "guess"+"\(indexPath)")
        tableViewCell.textLabel?.text = guesses[indexPath.row]
        print(tableViewCell.textLabel?.text)
        tableViewCell.textLabel?.font = UIFont(name: "orange juice", size: 29)
        
        return tableViewCell
    }
    
    

}
